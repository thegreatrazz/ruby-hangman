# frozen_string_literal: true

require "set"

# Represents an instance of a hangman game
class Hangman
  attr_reader :word, :discovered,
              :attempts, :used_letters

  ATTEMPT_COUNT = 6

  GUESS_FAIL = :fail            # the guess was not correct
  GUESS_OK = :ok                # the guess was correct
  GUESS_WIN = :win              # the game was won
  GUESS_GAMEOVER = :gameover    # the game was lost
  GUESS_USED = :used            # the letter was already used

  def initialize(word)
    @word = word.strip.downcase
    @discovered = Array.new(word.size)
    @attempts = ATTEMPT_COUNT
    @used_letters = Set[]
  end

  def guess(char)
    return GUESS_USED if @used_letters.include? char

    @used_letters << char

    # find all characters in the word
    ok_chars = 0
    @word.chars.each.with_index do |word_char, index|
      if word_char == char
        ok_chars += 1
        @discovered[index] = char
      end
    end

    if ok_chars.zero?
      @attempts -= 1
      return :fail if @attempts.positive?
    end

    return :gameover if @attempts.zero?
    return :win if discovered_word

    :ok   # return :ok if we're good
  end

  def discovered_word
    @discovered.inject(0) { |sum, n| sum + (n.nil? ? 0 : 1) } == @word.size
  end

  def figure
    case @attempts
    when 6
      <<~DRAW
        ─┬───┐
             │
             │
             │
             ╧
      DRAW
    when 5
      <<~DRAW
        ─┬───┐
         O   │
             │
             │
             ╧
      DRAW
    when 4
      <<~DRAW
        ─┬───┐
         O   │
         │   │
             │
             ╧
      DRAW
    when 3
      <<~DRAW
        ─┬───┐
         O   │
        /│   │
             │
             ╧
      DRAW
    when 2
      <<~DRAW
        ─┬───┐
         O   │
        /│\\  │
             │
             ╧
      DRAW
    when 1
      <<~DRAW
        ─┬───┐
         O   │
        /│\\  │
        /    │
             ╧
      DRAW
    when 0
      <<~DRAW
        ─┬───┐
         O   │
        /│\\  │
        / \\  │
             ╧
      DRAW
    end
  end

  def self.new_with_random_word
    dict = []
    File.foreach("#{File.dirname(__FILE__)}/../data/words.txt") { |line| dict << line.strip }

    Hangman.new dict.sample
  end
end
